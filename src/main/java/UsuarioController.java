import java.util.HashMap;
import java.util.List;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

public class UsuarioController {

   // método de autenticación del login
   public static Route auth = (Request req, Response res) -> {
      HashMap<String, Object> model = new HashMap<>();
      if(!req.queryParams("email").isBlank() && !req.queryParams("password").isBlank()){
         UsuarioDAO uDao = new UsuarioDAO();
         MenuDAO mDao = new MenuDAO();
         List<Usuario> users = uDao.getUsuario(req.queryParams("email"), req.queryParams("password"));
         
         if(users.size() > 0) {
            Usuario user = users.get(0);
            // creo la sesion y asigno los atributos
            req.session(true); 
            req.session().attribute("id", String.valueOf(user.getIdUsuario())); 
            req.session().attribute("nombre", user.getNombre()); 
            req.session().attribute("apellido", user.getApellido());
            req.session().attribute("mail", user.getMail());
            req.session().attribute("admin", user.getAdmin().toString());
            
            res.redirect("/index");
         } else {
            model.put("menu", mDao.getIndexMenu());
            model.put("template", "templates/loginForm.vtl");
            model.put("request", req);
            model.put("error", "La contraseña o el email es incorrecto / usuario inexistente");
         }
      }
      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };

   // cliente: trae la pantalla que permite añadir al carrito
   public static Route getCatalogoCompra = (Request req, Response res) -> {
      HashMap<String,Object> model = new HashMap<>();

      PastaDAO pDAO   = new PastaDAO();
      MasaDAO mDAO    = new MasaDAO();
      RellenoDAO rDAO = new RellenoDAO();
      MenuDAO menuDAO = new MenuDAO();

      String admin = req.session().attribute("admin");
      String nombre = req.session().attribute("nombre");
      String apellido = req.session().attribute("apellido");
      String mail = req.session().attribute("mail");
      String id = req.session().attribute("id");

      model.put("apellido", apellido);
      model.put("nombre", nombre);
      model.put("admin",admin);
      model.put("mail", mail);
      model.put("id-usuario", id);
      
      model.put("pastas", pDAO.getPastas());
      model.put("masas", mDAO.getMasas());
      model.put("rellenos", rDAO.getRellenos());
      model.put("menu", menuDAO.getClientMenu());

      model.put("template", "templates/catalogoCompra.vtl");

      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };

   public static Route logout = (Request req, Response res) -> {
      req.session().removeAttribute("admin");
      req.session().removeAttribute("nombre");
      req.session().removeAttribute("apellido");
      req.session().removeAttribute("mail");
      req.session().removeAttribute("id");
      res.redirect("/");
      return null;
   };

}
