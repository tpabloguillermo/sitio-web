import lombok.Data;

@Data
public class Usuario {
   protected int idUsuario;
   protected String nombre;
   protected String apellido;
   protected String mail;
   protected Boolean admin;

   public Usuario(){}
}
