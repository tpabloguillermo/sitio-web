import lombok.Data;

@Data
public class Relleno {
   protected int idRelleno;
   protected String nombreRelleno;
   protected String descripcion;

   public Relleno(){}
}
