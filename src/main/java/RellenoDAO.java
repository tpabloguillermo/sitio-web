import java.util.List;

import org.sql2o.Connection;

import util.Sql2oDAO;

public class RellenoDAO {
   public RellenoDAO() {}

   public List<Relleno> getRellenos() {
      List<Relleno> res = null;
      try(Connection con = Sql2oDAO.getSql2o().open()) {
         String sql = "SELECT * FROM Rellenos";
         res = con.createQuery(sql)
         .executeAndFetch(Relleno.class);
      } catch (Exception e) {
         System.out.println(e);
      }
      return res;
   }
   
   public Relleno getRellenoXid(int id){
      List<Relleno> res = null;
      try(Connection con = Sql2oDAO.getSql2o().open()) {
         String sql = "SELECT * FROM Rellenos WHERE idRelleno = :id";
         res = con.createQuery(sql)
         .addParameter("id", id)
         .executeAndFetch(Relleno.class);
      } catch (Exception e) {
         System.out.println(e);
      }
      return res.get(0);
   }
}
