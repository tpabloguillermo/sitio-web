import lombok.Data;

@Data
public class Menu {
   protected String nombre;
   protected String link;

   public Menu(String nombre, String link){
      this.nombre = nombre;
      this.link = link;
   } 
}
