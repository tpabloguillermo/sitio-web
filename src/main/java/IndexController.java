import java.util.HashMap;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

public class IndexController {

   /*
      Esto se encarga de redirigir fijarse que tipo de usuario es y seleccionar el menu
      que le corresponda
   */
   public static Route getIndex = (Request req, Response res) -> {
      HashMap<String,Object> model = new HashMap<>();
      MenuDAO mDao = new MenuDAO();
      CarritoDAO cDAO = new CarritoDAO();
   
      String admin = req.session().attribute("admin");
      String nombre = req.session().attribute("nombre");
      String apellido = req.session().attribute("apellido");
      String id = req.session().attribute("id");

      model.put("apellido", apellido);
      model.put("nombre", nombre);
      model.put("admin", admin);
      model.put("id-usuario",id);


      // me fijo que tipo de usuario es (admin o cliente) y traigo el menu corerspondiente
      if(admin != null) {
         if(admin.equals("true")) {
            model.put("menu", mDao.getAdminMenu());
            model.put("template", "templates/pantallaAdmin.vtl");
         } else {
            // si este usuario no tiene un carrito le asigno a la tabla carrito en la BD el id del user
            if(!cDAO.existsCarrito(Integer.parseInt(req.session().attribute("id")))) {
               cDAO.crearCarrito(Integer.parseInt(req.session().attribute("id")));
            }
            model.put("menu", mDao.getClientMenu());
            model.put("template", "templates/index.vtl");
         }
      } else {
         res.redirect("/");
      }

      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };
}
