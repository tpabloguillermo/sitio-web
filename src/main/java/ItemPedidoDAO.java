import org.sql2o.Connection;

import util.Sql2oDAO;

public class ItemPedidoDAO {
   public ItemPedidoDAO() {
   }

   public void setItemPedido(ItemPedido ip) {
      try (Connection con = Sql2oDAO.getSql2o().open()) {
         String insercion = "INSERT INTO ItemPedido VALUES(NULL, '" 
         + ip.getRellenos_idRelleno() + "','"
         + ip.getPastas_idPasta() + "','" 
         + ip.getMasas_idMasa() + "','" 
         + ip.getCantidad() + 
         "', NULL ,'" 
         + ip.getCarrito_Usuarios_idUsuario()+ "')";
         
         con.createQuery(insercion).executeUpdate();
      } catch (Exception e) {
         System.out.println(e);
      }
   }
}
