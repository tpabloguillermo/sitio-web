import java.util.HashMap;
import java.util.List;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;


public class PastaController {
    public static Route pastas = (Request req, Response res) -> {
        HashMap <String, Object> model = new HashMap<>();
        PastaDAO pDao = new PastaDAO();
        List<Pasta> pastas = pDao.getPastas();

        model.put("pastas", pastas);
        model.put("template", "templates/listarPastas.vtl");
        return new VelocityTemplateEngine().render(new ModelAndView(model, null));
    };                                                                                                                                                                                                                                                                                                     

    //Añadir nueva pastas   
    public static Route addPasta = (Request req, Response res) -> {
        HashMap <String, Object> model = new HashMap<>();
        MenuDAO mDao = new MenuDAO();
        model.put("nombre",req.session().attribute("nombre"));
        model.put("apellido",req.session().attribute("apellido"));
        model.put("admin",req.session().attribute("admin"));
        if (req.queryParams("enviar") != null) {
            Pasta pasta = new Pasta();
            boolean correcto = false;
            boolean error = false;
            try {
                pasta.setNombrePasta(req.queryParams("nombrepasta"));
                pasta.setDescripcion(req.queryParams("descripcion"));
                pasta.setPrecioVenta(Integer.parseInt(req.queryParams("precioventa")));
                pasta.setStock(Integer.parseInt(req.queryParams("stock")));
                PastaDAO pDao = new PastaDAO();
                correcto = pDao.addPasta(pasta);
            } catch (Exception e) {
                System.out.println(e);
                error = true;
                res.redirect("/index");
            }
            if(correcto){
                res.redirect("/index");
            }
            else{
                model.put("menu",mDao.getAdminMenu());
                model.put("error",error);
            }
        }else{
            model.put("menu",mDao.getAdminMenu());
            model.put("pasta", pastas);
            model.put("template","templates/addPasta.vtl");
        }    

        return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
    };
}
