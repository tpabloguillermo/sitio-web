import java.util.List;

import org.sql2o.Connection;
import util.Sql2oDAO;

public class MasaDAO {
   public MasaDAO(){}

   public List<Masa> getMasas(){
      List<Masa> res = null;
      try(Connection con = Sql2oDAO.getSql2o().open()) {
         String sql = "SELECT * FROM Masas";
         res = con.createQuery(sql)
         .executeAndFetch(Masa.class);
      } catch (Exception e) {
         System.out.println(e);
      }
      return res;
   }

   public Masa getMasaXid(int id){
      List<Masa> res = null;
      try(Connection con = Sql2oDAO.getSql2o().open()) {
         String sql = "SELECT * FROM Masas WHERE idMasa = :id";
         res = con.createQuery(sql)
         .addParameter("id", id)
         .executeAndFetch(Masa.class);
      } catch (Exception e) {
         System.out.println(e);
      }
      return res.get(0);
   }
}
