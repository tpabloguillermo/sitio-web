import java.util.List;

import org.sql2o.Connection;

import util.Sql2oDAO;

public class CarritoDAO {
   public CarritoDAO() {
   }

   // me dice si el usuario identificado con su ID tiene un carrito creado
   public boolean existsCarrito(int id) {
      List<Carrito> carrito = null;
      Boolean b = false;
      try (Connection con = Sql2oDAO.getSql2o().open()) {
         String sql = "SELECT * FROM Carrito where Usuarios_idUsuario = :id";
         carrito = con.createQuery(sql)
         .addParameter("id",id)
         .executeAndFetch(Carrito.class);
         if (carrito.size() > 0) {
            b = true;
         }
      } catch (Exception e) {
         System.out.println(e);
      }
      return b;
   }

   // crea el carrito para el usuario identificado con la ID
   public void crearCarrito(int id) {
      try (Connection con = Sql2oDAO.getSql2o().open()) {
         String sql = "INSERT INTO Carrito VALUES('"+id+"')";
         con.createQuery(sql).executeUpdate();
      } catch (Exception e) {
         System.out.println(e);
      }
   }

   // trae los elementos del carrito
   public List<ItemPedido> getElementosCarrito(int id){
      List<ItemPedido> res = null;
      try (Connection con = Sql2oDAO.getSql2o().open()) {
         String sql = "SELECT idItemPedido, Rellenos_idRelleno, Pastas_idPasta, Masas_idMasa, cantidad FROM ItemPedido WHERE Carrito_Usuarios_idUsuario = :id";
         res = con.createQuery(sql)
         .addParameter("id",id)
         .executeAndFetch(ItemPedido.class);
      } catch (Exception e) {
         System.out.println(e);
      }
      return res;
   }

   // añade un elemento al carrito
   public void setItemPedido(ItemPedido ip) {
      try (Connection con = Sql2oDAO.getSql2o().open()) {
         String insercion = "INSERT INTO ItemPedido VALUES(NULL, '" 
         + ip.getRellenos_idRelleno() + "','"
         + ip.getPastas_idPasta() + "','" 
         + ip.getMasas_idMasa() + "','" 
         + ip.getCantidad() + 
         "', NULL ,'" 
         + ip.getCarrito_Usuarios_idUsuario()+ "')";
         
         con.createQuery(insercion).executeUpdate();
      } catch (Exception e) {
         System.out.println(e);
      }
   }
}
