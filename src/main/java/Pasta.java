import lombok.Data;

@Data
public class Pasta{
    protected int idPasta;
    protected int precioVenta;
    protected int stock;
    protected String img;
    protected String descripcion;
    protected String nombrePasta;

    public Pasta(){
        this.idPasta=0;
        this.nombrePasta="";
        this.precioVenta=0;
        this.descripcion="";
        this.stock=0;
        this.img="";
    }
}