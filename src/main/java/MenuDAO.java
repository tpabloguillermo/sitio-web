import java.util.ArrayList;
import java.util.List;

public class MenuDAO {
   List<Menu> menu;

   public MenuDAO(){
      this.menu = new ArrayList<>();
   }

   public List<Menu> getAdminMenu() {
      Menu m = new Menu("Home","/index");
      menu.add(m);
      m = new Menu("Logout","/logout");
      menu.add(m);
      return this.menu;
   }

   public List<Menu> getClientMenu() {
      Menu m = new Menu("Home","/index");
      menu.add(m);
      m = new Menu("Pedidos","/misPedidos");
      menu.add(m);
      m = new Menu("Catalogo","/catalogoCompra");
      menu.add(m);
      m = new Menu("Logout","/logout");
      menu.add(m);
      return this.menu;
   }

   public List<Menu> getIndexMenu() {
      Menu m = new Menu("Home","/");
      menu.add(m);
      m = new Menu("Login","/loginForm");
      menu.add(m);
      return this.menu;
   }
}
