import lombok.Data;

@Data
public class ItemPedido {
   protected int idItemPedido;
   protected int Rellenos_idRelleno;
   protected int Pastas_idPasta;
   protected int Masas_idMasa;
   protected int cantidad;
   protected int Pedidos_nroPedido;
   protected int Carrito_Usuarios_idUsuario;

   // estos objetos los uso cuando trabajo con el vtl
   protected Relleno relleno;
   protected Pasta pasta;
   protected Masa masa;

   public ItemPedido(){
      this.idItemPedido = 0;
      this.Rellenos_idRelleno = 0;
      this.Pastas_idPasta = 0;
      this.Masas_idMasa = 0;
      this.cantidad = 0;
      this.Pedidos_nroPedido = 0;
      this.Carrito_Usuarios_idUsuario = 0;
   }
}
