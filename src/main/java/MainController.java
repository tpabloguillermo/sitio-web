import java.util.HashMap;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;


public class MainController {
   
   // página index, es lo que se muestra antes de loguearse
   public static Route getWelcomePage = (Request req, Response res) -> {
      HashMap<String,Object> model = new HashMap<>();
      MenuDAO mDao = new MenuDAO();
      model.put("menu", mDao.getIndexMenu());
      model.put("template", "templates/index.vtl");
      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };

   // formulario de logueo
   public static Route loginForm = (Request req, Response res) -> {
      HashMap<String,Object> model = new HashMap<>();
      MenuDAO mDao = new MenuDAO();
      model.put("menu", mDao.getIndexMenu());
      model.put("template", "templates/loginForm.vtl");
      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };

}
