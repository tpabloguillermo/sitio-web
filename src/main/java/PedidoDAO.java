import java.util.List;

import org.sql2o.Connection;
import util.Sql2oDAO;

public class PedidoDAO {
   public PedidoDAO(){}
   
   // devuelve todos los pedidos
   public List<Pedido> getPedidos(){
      String sql = "SELECT * FROM Pedidos";
      List<Pedido> res = null;
      try (Connection con = Sql2oDAO.getSql2o().open()){
         res = con.createQuery(sql)
         .executeAndFetch(Pedido.class);

         for(int i = 0; i<res.size(); i++ ){
            List<ItemPedido> il = getItemsxPedido(res.get(i).getNroPedido());
            res.get(i).setItemsPedido(il);
         }
         
      } catch (Exception e) { 
         System.out.println(e);
      }
      return res;
   }

   // devuelve todos los items que tiene un pedido en específico
   public List<ItemPedido> getItemsxPedido(int nroPedido){
      String sql = "SELECT * FROM ItemPedido WHERE Pedidos_nroPedido = :nroPedido";
      List<ItemPedido> res = null;
      try (Connection con = Sql2oDAO.getSql2o().open()){
         res = con.createQuery(sql)
         .addParameter("nroPedido",nroPedido)
         .executeAndFetch(ItemPedido.class);
         MasaDAO mDao = new MasaDAO();
         RellenoDAO rDao = new RellenoDAO();
         PastaDAO pDao = new PastaDAO();
         for(ItemPedido i : res){
            i.setMasa(mDao.getMasaXid(i.getMasas_idMasa()));
            i.setRelleno(rDao.getRellenoXid(i.getRellenos_idRelleno()));
            i.setPasta(pDao.getPastaXid(i.getPastas_idPasta()));
         }
      } catch (Exception e) {
         System.out.println(e);
      }
      return res;
   }

   // recupera los pedidos realizados por un usuario
   public List<Pedido> getPedidosUsuario(int idUsuario){
      String sql = "SELECT * FROM Pedidos WHERE Usuarios_idUsuario = :idUsuario";
      List<Pedido> res = null;
      try (Connection con = Sql2oDAO.getSql2o().open()){
         res = con.createQuery(sql)
         .addParameter("idUsuario", idUsuario)
         .executeAndFetch(Pedido.class);
         for(int i = 0; i<res.size(); i++ ){
            List<ItemPedido> il = getItemsxPedido(res.get(i).getNroPedido());
            res.get(i).setItemsPedido(il);
         }
         
      } catch (Exception e) { 
         System.out.println(e);
      }
      return res;
   }

   // update a un item de la base de datos
   public void updateItem(ItemPedido ip) {
      String sql = "UPDATE `ItemPedido` SET `Rellenos_idRelleno` = '"+ ip.getRellenos_idRelleno() + "', `Pastas_idPasta` = '" +ip.getPastas_idPasta() + "', `Masas_idMasa` = '"+ip.getMasas_idMasa()+"', `cantidad` = '"+ip.getCantidad()+"' WHERE `ItemPedido`.`idItemPedido` = "+ip.getIdItemPedido()+";";
      try (Connection con = Sql2oDAO.getSql2o().open()){ 
         con.createQuery(sql).executeUpdate();
      } catch(Exception e) {
         System.out.println(e);
      }
   }
}
