
import java.sql.Date;
import java.util.List;

import lombok.Data;


@Data
public class Pedido {
   protected int nroPedido;
   protected int Usuarios_idUsuario;
   protected int Pastas_idPastas;
   protected int cantidad;
   protected Date fechaPedido;
   protected Date fechaEntrega;
   protected String direccionEnvio;
   protected Boolean entregado;
   protected List<ItemPedido> itemsPedido;

   public Pedido() {
      this.entregado = false;
   }
}
