import java.util.List;

import org.sql2o.Connection;
import util.Sql2oDAO;

public class PastaDAO {
    public PastaDAO() {
    }

    public List<Pasta> getPastas() {
        String sql = "SELECT * FROM Pastas";
        List<Pasta> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            res = con.createQuery(sql).executeAndFetch(Pasta.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return res;
    }

    public boolean addPasta(Pasta pasta) {
        boolean correcto = false;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            String insercion = "INSERT INTO Pastas VALUES(NULL, '" + pasta.getNombrePasta() + "','"
                    + pasta.getPrecioVenta() + "','" + pasta.getDescripcion() + "','" + pasta.getStock() + "')";
            con.createQuery(insercion).executeUpdate();
            correcto = true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return correcto;
    }

    public Pasta getPastaXid(int id) {
        List<Pasta> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            String sql = "SELECT * FROM Pastas WHERE idPasta = :id";
            res = con.createQuery(sql).addParameter("id", id).executeAndFetch(Pasta.class);

        } catch (Exception e) {
            System.out.println(e);
        }
        return res.get(0);
    }
}