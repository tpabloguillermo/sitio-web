import lombok.Data;

@Data
public class Masa {
   protected int idMasa;
   protected String nombreMasa;
   protected String descripcion;

   public Masa(){}
}
