import java.util.List;

import org.sql2o.Connection;
import util.Sql2oDAO;

public class UsuarioDAO {
   
   public List<Usuario> getUsuario(String email, String password) {
      String sql = "SELECT idUsuario,nombre,apellido,mail,admin FROM Usuarios WHERE mail = :email AND password = :password";
      
      List<Usuario> res = null;
      try (Connection con = Sql2oDAO.getSql2o().open()){
         res = con.createQuery(sql)
               .addParameter("email",email)
               .addParameter("password", password)
               .executeAndFetch(Usuario.class);
      } catch (Exception e) { 
         System.out.println(e);
      }  
      return res;
   }
}
