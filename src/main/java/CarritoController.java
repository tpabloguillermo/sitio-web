import java.util.HashMap;
import java.util.List;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

public class CarritoController {

   // crea un item de pedido
   public static Route añadirCarrito = (Request req, Response res) -> { 
      HashMap<String,Object> model = new HashMap<>();
      CarritoDAO carritoDAO = new CarritoDAO();
      ItemPedido ip = new ItemPedido();

      ip.setMasas_idMasa(Integer.parseInt(req.queryParams("masa")));
      ip.setPastas_idPasta(Integer.parseInt(req.queryParams("pasta")));
      ip.setRellenos_idRelleno(Integer.parseInt(req.queryParams("relleno")));
      ip.setCantidad(Integer.parseInt(req.queryParams("cantidad")));
      // id de usuario es el mismo que el id de carrito 
      ip.setCarrito_Usuarios_idUsuario(Integer.parseInt(req.session().attribute("id")));

      carritoDAO.setItemPedido(ip);
      
      res.redirect("/catalogoCompra");

      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };

   // método que se encarga de recuperar los elementos del carrito del usuario correspondiente
   public static Route verCarrito = (Request req, Response res) -> {  
      HashMap<String,Object> model = new HashMap<>();

      CarritoDAO cDAO = new CarritoDAO();
      MenuDAO mDAO = new MenuDAO();
      MasaDAO masaDAO = new MasaDAO();
      RellenoDAO rellenoDAO = new RellenoDAO();
      PastaDAO pastaDAO = new PastaDAO();
      
      String admin = req.session().attribute("admin");
      String nombre = req.session().attribute("nombre");
      String apellido = req.session().attribute("apellido");
      String mail = req.session().attribute("mail");
      String id = req.session().attribute("id");

      model.put("apellido", apellido);
      model.put("nombre", nombre);
      model.put("admin",admin);
      model.put("mail", mail);
      model.put("id-usuario", id);
      
      // traigo los elementos del carrito
      List<ItemPedido> carrito = cDAO.getElementosCarrito(Integer.parseInt(req.session().attribute("id")));

      if(carrito.size()>0){
         // traigo los nombres y descripciones de cada uno (id ya lo tengo)
         for(ItemPedido i: carrito) {
            i.setMasa(masaDAO.getMasaXid(i.getMasas_idMasa()));
            i.setRelleno(rellenoDAO.getRellenoXid(i.getRellenos_idRelleno()));
            i.setPasta(pastaDAO.getPastaXid(i.getPastas_idPasta()));
         }
         model.put("template", "templates/carrito.vtl");
         model.put("carrito", carrito);
      } else {
         model.put("template", "templates/carritoVacio.vtl");
      }

      model.put("menu", mDAO.getClientMenu());
      
      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };
}
