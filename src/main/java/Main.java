import static spark.Spark.get;
import static spark.Spark.post;
import static spark.debug.DebugScreen.enableDebugScreen;
import static spark.Spark.staticFiles; 



public class Main {
   public static void main(String[] args) {
      enableDebugScreen();
      staticFiles.location("/public");

      // general
      get("/", MainController.getWelcomePage);
      get("/loginForm", MainController.loginForm);
      get("/index", IndexController.getIndex);
      get("/logout", UsuarioController.logout);

      // cliente
      get("/catalogoCompra", UsuarioController.getCatalogoCompra);
      get("/verCarrito", CarritoController.verCarrito);
      get("/misPedidos", PedidoController.getPedidosUsuario);
      get("/modificarPedido", PedidoController.modificarPedido);
      post("/addCarrito", CarritoController.añadirCarrito);
      post("/guardarPedido", PedidoController.guardarPedido);


      // admin
      get("/getPedidos", PedidoController.getPedidos);
      get("/addPasta", PastaController.addPasta);
      post("/addPasta", PastaController.addPasta);

      // autentificación
      get("/login", UsuarioController.auth);

   }
}
   