import java.util.HashMap;
import java.util.List;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

public class PedidoController {

   // admin: traer todos los pedidos pendientes
   public static Route getPedidos = (Request req, Response res) -> {
      HashMap <String, Object> model = new HashMap<>();
      PedidoDAO pDao = new PedidoDAO();
      List<Pedido> pedidos = pDao.getPedidos();
      MenuDAO mDao = new MenuDAO();
      model.put("menu",mDao.getAdminMenu());

      model.put("nombre",req.session().attribute("nombre"));
      model.put("apellido",req.session().attribute("apellido"));
      model.put("admin",req.session().attribute("admin"));
      if(pedidos.size()>0 && pedidos != null) {

         model.put("pedidos", pedidos);
         model.put("template", "templates/pedidosTable.vtl");
      } else {
         // vista que dice que NO HAY PEDIDOS PENDIENTES
         model.put("template", "templates/sinPedidos.vtl");
      }
      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };

   // cliente: ver los pedidos de un usuario específico
   public static Route getPedidosUsuario = (Request req, Response res) -> {
      HashMap <String, Object> model = new HashMap<>();
      PedidoDAO pDao = new PedidoDAO();
      List<Pedido> pedidos = pDao.getPedidosUsuario(Integer.parseInt(req.session().attribute("id")));
      MenuDAO mDao = new MenuDAO();

      model.put("menu",mDao.getClientMenu());
      model.put("nombre",req.session().attribute("nombre"));
      model.put("apellido",req.session().attribute("apellido"));
      model.put("admin",req.session().attribute("admin"));

      if(pedidos.size()>0 && pedidos != null) {
         model.put("pedidos", pedidos);
         model.put("template", "templates/pedidosTable.vtl");
      } else {
         // vista que dice que NO HAY PEDIDOS PENDIENTES
         model.put("template", "templates/sinPedidos.vtl");
      };
      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };

   // cliente: muestra la tarjetita para modificar el pedido
   public static Route modificarPedido = (Request req, Response res) -> {
      HashMap <String, Object> model = new HashMap<>();
      
      PastaDAO pDAO   = new PastaDAO();
      MasaDAO mDAO    = new MasaDAO();
      RellenoDAO rDAO = new RellenoDAO();
      MenuDAO menuDAO = new MenuDAO();

      model.put("menu",menuDAO.getClientMenu());
      model.put("nombre",req.session().attribute("nombre"));
      model.put("apellido",req.session().attribute("apellido"));
      model.put("admin",req.session().attribute("admin"));
      

      model.put("idItemPedido", req.queryParams("idItemPedido"));
      model.put("pastas", pDAO.getPastas());
      model.put("masas", mDAO.getMasas());
      model.put("rellenos", rDAO.getRellenos());

      model.put("template","templates/tarjetaParaModificar.vtl");

      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl")); 
   };

   public static Route guardarPedido = (Request req, Response res) -> {
      HashMap <String, Object> model = new HashMap<>();
      PedidoDAO pDAO = new PedidoDAO();
      ItemPedido ip = new ItemPedido();

      ip.setIdItemPedido(Integer.parseInt(req.queryParams("idItemPedido")));
      ip.setMasas_idMasa(Integer.parseInt(req.queryParams("masa")));
      ip.setPastas_idPasta(Integer.parseInt(req.queryParams("pasta")));
      ip.setCantidad(Integer.parseInt(req.queryParams("cantidad")));
      ip.setRellenos_idRelleno(Integer.parseInt(req.queryParams("relleno")));

      pDAO.updateItem(ip);

      res.redirect("/misPedidos");
      return new VelocityTemplateEngine().render(new ModelAndView(model, "templates/layout.vtl"));
   };
}
